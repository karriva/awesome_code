import math
import sys

# Numbers - неизменяемый тип данных
# Работа с числом

print(bin(17))

print(hex(17))

print(math.sqrt(17))

print(17 % 5)

print(round(17/3))

# Strings - неизменяемый тип данных
# Работа со строкой

test_str = "mkmkmklmlma"
print(test_str[0], test_str[-1])
print(len(test_str))
print(test_str[2:6])
print(test_str.upper())
print(test_str.count('m'))
print(test_str.replace('ma', 'am'))
print(test_str.split('m'))

# List - изменяемый тип данных
# Ввод списков

example_list_1 = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15']
example_list_2 = ['List', '2', '5', '6', '8', 'comment', 'place', '67', '89', '12']
example_list_3 = list('12g67H7899fhi67bj89ebj678byn56nik78')
example_list_3 = example_list_3 * 2

# Длинна списка

print(len(example_list_1))
print(len(example_list_2))
print(len(example_list_3))

# Объединение двух списков

print(example_list_1+example_list_2)

# Вывод на экран последние элементы списка.

print(example_list_1[14])
print(example_list_2[9])
print(example_list_3[69])

print(''.join(example_list_2))

# Добавляем элемент в конец списка

example_list_1.append('8')
example_list_2.append('8')
print(example_list_1)
print(example_list_2)

# Удаление элементов
example_list_3.pop(0)
example_list_3.pop(-1)
print(example_list_3)

# Сортировка списка

print(sorted(example_list_3))

# Dictionary - изменяемый тип данных

London = {'name': 'London1', 'location': 'London Str', 'vendor': 'Cisco'}

London_co = {
    'r1': {
        'hostname': 'london_r1',
        'location': '21 New Globe Walk',
        'vendor': 'Cisco',
        'model': '4451',
        'ios': '15.4',
        'ip': '10.255.0.1'
    },
    'r2': {
        'hostname': 'london_r2',
        'location': '21 New Globe Walk',
        'vendor': 'Cisco',
        'model': '4451',
        'ios': '15.4',
        'ip': '10.255.0.2'
    },
    'sw1': {
        'hostname': 'london_sw1',
        'location': '21 New Globe Walk',
        'vendor': 'Cisco',
        'model': '3850',
        'ios': '3.6.XE',
        'ip': '10.255.0.101'
    }
}
# Выводим количество элементов каждого словаря.

print(len(London))
print(len(London_co))

# Выводим только ключи словарей.

print(London.keys())
print(London_co.keys())

# Значение ключа

print(London.get('vendor'))

# Добавляем в словарь пару.

London.setdefault('meta', 'object')
print(London)

# Вывод значений словаря.

print(London.values())

# Вывод всех пар словаря.

print(London.items())

# Удаление ключа.

del London_co['sw1']

# Обновление значения ключа

London_co.update({'r2': '1234'})
print(London_co)

# Tuple - неизменяемый упорядоченный тип данных

tuple1 = tuple()

list1 = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']
tuple2 = tuple(list1)
print(tuple2[0])

# Set - изменяемый тип данных

vl1 = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
set1 = set(vl1)
set1.add(30)
set1.discard(40)

vl2 = [18, 19, 20, 21, 22, 23, 24, 25]
set2 = set(vl2)

print(set1 & set2)

# Bool

a = [1, 4, 7]
b = [0]

bool(a == b)

list_1 = []
bool(a == list_1)
bool(b == list_1)

bool('khjhj' == a)
bool('khjhj' == b)

empty_value = None

bool(empty_value == a)
bool(empty_value == b)

# Задача со звёздочкой

List1 = list('0123456789')
Set1 = set(List1)
Tuple1 = tuple(List1)

print(sys.getsizeof(List1))
print(sys.getsizeof(Set1))
print(sys.getsizeof(Tuple1))

print(type(List1))
print(type(Set1))
print(type(Tuple1))
