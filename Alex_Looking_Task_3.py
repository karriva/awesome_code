ip_adress = input("Введите IP-адрес в формате 10.0.1.1")

ip_adress_list = ip_adress.split(".")

if ip_adress == "255.255.255.255":
    print("local broadcast")
elif ip_adress == "0.0.0.0":
    print("unassigned")
elif int(ip_adress_list[0]) > 0 and int(ip_adress_list[0]) < 224:
    print("unicast")
elif int(ip_adress_list[0]) > 223 and int(ip_adress_list[0]) <= 239:
    print("multicast")
else:
    print("unused")
