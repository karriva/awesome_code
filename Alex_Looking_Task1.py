# Числа
import math

bin(17)
hex(17)
math.sqrt(17)
17/5
17/3
17//3

# Строки
testLine = "mkmkmklmlma"
print(testLine[0], testLine[-1])
print(len(testLine))
print(testLine[1:5])
testLine.upper()
testLine.count("m")

testLine2 = testLine[0:-2] + testLine[-1] + testLine[-2]

testLine.split("m")

print(testLine[-1:-3])

# Списки
example_list_1 = list()
for i in range(15):
    example_list_1.append(i+1)

# example_list_1 = list()
# example_list_1 = [i+1 for i in range(15)]

example_list_2 = [13, 2, "arkansas", 234, "charnley", "forest", 86, 93, 1, 534, "washita"]
Line = "PyCharm is a dedicated Python Integrated Development Environment (IDE) providing a wide"
example_list_3 = list(Line)

print("Длина первого списка", len(example_list_1))
print("Длина второго списка", len(example_list_2))
print("Длина третьего списка", len(example_list_3))

print(example_list_1+example_list_2)

print(example_list_1[-1], example_list_2[-1], example_list_3[-1])

example_list_2_2=list()
for i in range(len(example_list_2)):
    example_list_2_2 += str(example_list_2[i])
print(''.join(example_list_2_2))

example_list_1.append('8'),
example_list_2.append('8')

example_list_3.pop(0)
example_list_3.pop()

example_list_3.sort()

# Словари

london = {'name': 'London1', 'location': 'London Str', 'vendor': 'Cisco'}

london_co = {
    'r1': {
        'hostname': 'london_r1',
        'location': '21 New Globe Walk',
        'vendor': 'Cisco',
        'model': '4451',
        'ios': '15.4',
        'ip': '10.255.0.1'
    },
    'r2': {
        'hostname': 'london_r2',
        'location': '21 New Globe Walk',
        'vendor': 'Cisco',
        'model': '4451',
        'ios': '15.4',
        'ip': '10.255.0.2'
    },
    'sw1': {
        'hostname': 'london_sw1',
        'location': '21 New Globe Walk',
        'vendor': 'Cisco',
        'model': '3850',
        'ios': '3.6.XE',
        'ip': '10.255.0.101'
    }
}

print(len(london.keys()))
print(len(london_co.keys()))

london_list = list(london.keys())
london_co_list = list(london_co.keys())
print(london_list)
print(london_co_list)

print(london['vendor'])

london.update({'meta': 'object'})

london_list2 = list(london.values())
print(london_list2)

print(london.items())

del london_co['sw1']

london_co.update({'r2': '1234'})

# Кортежи

example_tuple_1 = tuple()

example_tuple_2 = (1, 2, 3, 4, 5, 6, 7, 8, 8, 10, 11, 12)

print(example_tuple_2[0])

# Множества

example_set = {i+10 for i in range(11)}

example_set.add(30)

example_set.discard(40)

example_set2 = {i+18 for i in range(8)}

print(example_set.intersection(example_set2))

# Булевы

exam_bul = True
exam_bul2 = False

print(bool(exam_bul) == bool(exam_bul2))

if exam_bul == exam_bul2:
    print(True)
else:
    print(False)

exam_bul3 = []

if exam_bul == exam_bul3:
    print("True")
else:
    print("False")

if exam_bul2 == exam_bul3:
    print("True")
else:
    print("False")

if exam_bul == "khjhj":
    print("True")
else:
    print("False")

if exam_bul2 == "khjhj":
    print("True")
else:
    print("False")

empty_value = None

if exam_bul == empty_value:
    print("True")
else:
    print("False")

if exam_bul2 == empty_value:
    print("True")
else:
    print("False")